export let renderProductList = (proArr) => {
  let contentHTML = "";
  proArr.forEach((item) => {
    let contentTr = ` <tr>
                            <td>${item.id}</td>
                            <td>${item.name}</td>
                            <td>${item.price}</td>
                            <td>${item.screen}</td>
                            <td>${item.backCamera}</td>
                            <td>${item.frontCamera}</td>
                            <td>${item.img}</td>
                            <td>${item.desc}</td>
                            <td>${item.type}</td>
                            <td>
                            <button
                            onclick="deleteProduct(${item.id})"
                            class="btn btn-danger">Xoá</button>

                             <button
                            onclick="suaProduct(${item.id})"
                            class="btn btn-secondary">Sửa</button>
                            </td>
                      </tr>`;

    contentHTML += contentTr;
  });
  document.getElementById("tbodyProduct").innerHTML = contentHTML;
};

export let onSuccess = () => {
  Toastify({
    text: "Success",
    duration: 3000,
    className: "bg-dark text-white",
  }).showToast();
};

let onFail = () => {};

export let layThongTinTuForm = () => {
  let id = document.getElementById("maSP").value;
  let ten = document.getElementById("tenSP").value;
  let gia = document.getElementById("gia").value;
  let manHinh = document.getElementById("manHinh").value;
  let cameraTruoc = document.getElementById("cameraTruoc").value;
  let cameraSau = document.getElementById("cameraSau").value;
  let hinhAnh = document.getElementById("hinhAnh").value;
  let loai = document.getElementById("loaiMay").value;
  let moTa = document.getElementById("moTa").value;
  return {
    id,
    ten,
    gia,
    manHinh,
    cameraTruoc,
    cameraSau,
    hinhAnh,
    loai,
    moTa,
  };
};

export let showThongTinLenForm = (item) => {
  document.getElementById("maSP").value = item.id;
  document.getElementById("tenSP").value = item.name;
  document.getElementById("gia").value = item.price;
  document.getElementById("manHinh").value = item.screen;
  document.getElementById("cameraTruoc").value = item.backCamera;
  document.getElementById("cameraSau").value = item.frontCamera;
  document.getElementById("hinhAnh").value = item.img;
  document.getElementById("loaiMay").value = item.desc;
  document.getElementById("moTa").value = item.type;
};
