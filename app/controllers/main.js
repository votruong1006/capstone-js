import {
  layThongTinTuForm,
  onSuccess,
  renderProductList,
  showThongTinLenForm,
} from "./controller.js";

let BASE_URL = "https://63bea800f5cfc0949b5d49ca.mockapi.io/Product";

let fetchProductList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      renderProductList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProductList();

//XÓA SP
let deleteProduct = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      onSuccess();

      fetchProductList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.deleteProduct = deleteProduct;

//THÊM SP
let createProduct = () => {
  let product = layThongTinTuForm();
  axios({
    url: BASE_URL,
    method: "POST",
    data: product,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      onSuccess();
      fetchProductList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.createProduct = createProduct;

//SỬA SP
let suaProduct = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      onSuccess();
      showThongTinLenForm(res.data);
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.suaProduct = suaProduct;

//  CẬP NHẬT SP
let updateProduct = () => {
  let product = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/${product.id}`,
    method: "PUT",
    data: product,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      onSuccess();
      fetchProductList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.updateProduct = updateProduct;
